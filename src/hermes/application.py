#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import argparse
import datetime
from xmlrpclib import Fault
import sqlite3
from twisted.python import log
import json
import Queue

#for smpp-loging
import logging

from twisted.internet import reactor, ssl, defer, threads
from twisted.web import xmlrpc, server, resource
from twisted.python.failure import Failure
from twisted.internet.defer import inlineCallbacks, returnValue

#for amqp-rabbitmq interface
from twisted.internet.protocol import ClientCreator
from txamqp.client import TwistedDelegate
from txamqp.protocol import AMQClient
from txamqp.content import Content
import txamqp.spec

#for email
from email.mime.text import MIMEText
from email import message_from_string
from twisted.mail.smtp import sendmail

#for jabber
import xmpp

#for icq
from twisted.internet import protocol
from twisted.words.protocols import oscar

#for coding pdu (smpp)
from smpp.pdu.pdu_types import *
from smpp.pdu.operations import *
#for smpp sending
from smpp.twisted.client import ClientFactory, SMPPClientFactory, SMPPClientTransceiver
from smpp.twisted.config import SMPPClientConfig

def parseCommandLineOptions():
    """command line arguments to args structure init"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default = "config.json")
    args = parser.parse_args()
    return args.config
_Config_ = json.load(open(parseCommandLineOptions(),'r'))

class XmlRpcFacade(xmlrpc.XMLRPC):
    """XmlRpc facade for billing application"""
    def __init__(self, service):
        xmlrpc.XMLRPC.__init__(self)
        self._service = service

    def auth(self, user, passwd):
        return self._service.auth(user, passwd)

    def render_POST(self, request):
        """
        As we need to provide authorization, we add this override method
        @note: Overrides the xmlrpc.XMLRPC.render_POST. Sadly, we can't use
        super() as the xmlrpc.XMLRPC is the old style class :(
        @todo: use twisted.cred
        @param request:
        """
        from twisted.web import http
        user = request.getUser()
        passwd = request.getPassword()

        if user=="" and passwd=="":
            request.setResponseCode(http.UNAUTHORIZED)
            return "Authorization required!"

        if not self.auth(user, passwd):
            request.setResponseCode(http.UNAUTHORIZED)
            return "Authorization failed!"

        log.msg("request initiator: %s" % request.user)
        return xmlrpc.XMLRPC.render_POST(self, request)

    def _ebRender(self, failure):
        if isinstance(failure.value, Fault):
            return failure.value
        log.err(failure)
        failure_code = 100
        return Fault(failure_code, str(failure.value.message))

    def xmlrpc_sendMessage(self, object):
        if type(object) != dict:
            raise Exception('Error with input parameters!')
        log.msg("XMLRPC|%s|%s|%s" % (object.get("transport"), object.get("recipient", None), object.get("subj", None)))
        return self._service.push_queue(object.get("transport"),
                                        object.get("recipient", None),
                                        object.get("lang", "ENG"),
                                        object.get("subject", None),
                                        object.get("text"),
                                        "XMLRPC")

    def xmlrpc_getStatus(self, id):
        def getStatus(id):
            cursor = self._service.dblog.cursor()
            status, = cursor.execute("select status from queue where id=%s" % id).fetchone()
            cursor.close()
            return status
        log.msg("XMLRPC getStatus(%s)" % id)
        try:
            id = int(id)
        except:
            raise Exception('Error with task identifier! Must be integer value!')
        return getStatus(id)

class  AMQPFasade(object):
    def __init__(self, service):
        self._service = service
        self.init_client()

    def init_client(self):
        amqp_config = _Config_.get("listeners", None).get("amqp", None)
        if not amqp_config:
            raise "Attention! Config file has not amqp config info!!!"
        self._host = amqp_config.get("host")
        self._port = int(amqp_config.get("port"))
        self._username = amqp_config.get("username")
        self._password = amqp_config.get("password")
        self._vhost = amqp_config.get("vhost")
        self._queue = amqp_config.get("queue")
        spec = txamqp.spec.load(amqp_config.get("spec"))
        delegate = TwistedDelegate()
        d = ClientCreator(reactor, AMQClient, delegate=delegate, vhost=self._vhost, spec=spec).connectTCP(self._host, self._port)
        d.addCallback(self.gotConnection, self._username, self._password)
        d.addCallback(self.consume)
        d.addErrback(lambda f: sys.stderr.write(str(f)))

    @inlineCallbacks
    def gotConnection(self, connection, username, password):
        log.msg(" Got connection, authenticating.")
        # Older version of AMQClient have no authenticate
        # yield connection.authenticate(username, password)
        # This produces the same effect
        yield connection.start({"LOGIN": username, "PASSWORD": password})
        self.channel = yield connection.channel(1)
        yield self.channel.channel_open()
        returnValue((connection, self.channel))

    @inlineCallbacks
    def consume(self, result):
        connection, channel = result
        yield channel.queue_declare(queue=self._queue)
        msgnum = 0
        log.msg(" [*] AMQP-interface waiting for messages.")
        yield channel.basic_consume(queue=self._queue, no_ack=True, consumer_tag=self._queue)
        queue = yield connection.queue(self._queue)
        while True:
            msg = yield queue.get()
            msgnum += 1
            command = json.loads(msg.content.body)
            if type(command) is dict:
                log.msg("AMQP|[%04d] Received %r from channel #%d" % (msgnum, command, channel.id))
                request = command.get("request", None)
                if type(request) is dict:
                    reply = None
                    if msg.content.properties.get('reply to', None):
                        reply = msg.content.properties.get('reply to', None)
                    #its for alternate msg body with sending tool
                    elif command.get("properties", None):
                        reply = command.get("properties").get("reply to", None)
                    result = self._service.push_queue(request.get("transport"),
                                  request.get("recipient", None),
                                  request.get("lang", "ENG"),
                                  request.get("subject", None),
                                  request.get("text"),
                                  "AMPQ",
                                  reply, #routing key sender for send answer
                             )
                else:
                    log.msg("From AMQP channel got wrong data")
            else:
                log.msg("From AMQP channel got wrong data")
        returnValue(result)

    def send_answer(self, msg, label):
        channel = self.channel
        body = json.dumps(msg)
        return channel.basic_publish(routing_key=label, content=Content(body))

class ICQProtocol(oscar.BOSConnection):
    capabilities = [oscar.CAP_CHAT]

    def initDone(self):
        self.requestSSI().addCallback(self.gotBuddyList)

    def gotBuddyList(self, l):
        self.activateSSI()
        self.setProfile("ICQBot.py for  testing. Only sending - nor reading!!!")
        self.setIdleTime(0)
        self.clientReady()
        log.msg("Connecting to icq-server done")

    def gotAway(self, away, user):
        pass

    def receiveMessage(self, user, multiparts, flags):
        log.msg("Incomming message. From: %s. Message: %s" % (user.name, multiparts[0][0]))
        if str(user.name) != str(self.username):
            parrotmsg = "Привет! Это бот! В ближайшее время человек не будет читать переписку, а сообщения будут затираться\nhi from icq bot! real man cannot answer to you, and all messages will vanish!"
            self.sendMessage(user.name, parrotmsg.decode("utf-8").encode("cp1251"))

    def send(self, user, message):
        self.sendMessage(user, message)

class ICQIfaceAuth(oscar.OscarAuthenticator):
    BOSClass = ICQProtocol

    def __init__(self, username, password):
        oscar.OscarAuthenticator.__init__(self, username, password, icq=1)
        self.protocol = None

    def connectToBOS(self, server, port):
        p =  protocol.ClientCreator(reactor, self.BOSClass, self.username, self.cookie)
        if not self.protocol:
            self.protocol = p.connectTCP(server, int(port))
        return self.protocol

    def connectionLost(self, reason):
        log.msg("Connection to Icq Lost! %s \n\rOff icq interface or try later" % self)
#        reactor.callLater(5, reactor.connectTCP, _Config.get("sender", "icqserver"),int(_Config.get("sender", "icqport")), self.factory._service._bl.icqconn)
        reactor.stop()


class ICQFactory(ClientFactory):
    def __init__(self, username, password):
        self.username = username
        self.password = password
    def buildProtocol(self, addr):
        self.protocol = ICQIfaceAuth(self.username, self.password)
        self.protocol.factory = self
        return self.protocol
    def startedConnecting(self, connector):
        log.msg("Prepearing ICQ-connection done! %s" % connector)
    def clientConnectionFailed(self, connector, reason):
        log.msg("Clent connection to Icq failed! %s " % reason)
    def clientConnectionLost(self, connector, reason):
        log.msg("Clent connection to Icq Lost! %s" % reason)
    def stopFactory(self):
        log.msg("Stopping icq-connection")
#        self.protocol.transport.loseConnection()  #not effective for normal losing icq connection

class BusinessLogic(object):
    """Business logic of application realization"""
    def __init__(self):
        self.dblog    = None
        self.transports = {}

        self.tries = 3   #tries to failed task
        self.tasksQueue = Queue.Queue()
        reactor.callLater(30, self.__restore_queue)
        reactor.callLater(0, self.__queue_handler)

    def __getattr__(self, name):
        def handler(*args):
            try:
                if name not in dir(self):
                    raise Exception("Has no method(" + name + ")!!!")
                return getattr(self, name)(*args)
            except Exception, ex:
                log.msg(str(ex))
                raise ex
        name = '_' + name
        return handler

    def __getDone(self, p):
        return self.__logData(p)

    def __getFailed(self, f, method):
        try:
            f.trap([Failure, Exception])
        except Exception, ex:
            log.msg("%s exception: %s" % (method, str(ex)))
            log.msg(str(ex))
            raise ex
        except Failure, fa:
            log.msg("%s failure: %s" % (method, str(fa)))
            log.msg(str(fa))
            raise fa

    def __logData(self, stamp, type, recipient, result, subject, defer_result):
        """write text to log and return it"""
        log.msg("%s|%s|recipient: %s|result: %s|Message: %s|%s" % (stamp, type, recipient, result, subject, defer_result))
        return result

    def auth(self, user, passwd):
        return (user, passwd) == (_Config_.get("listeners").get("xmlrpc").get("username"),
                                  _Config_.get("listeners").get("xmlrpc").get("password"))

    def _push_queue(self, transport_name, recipient, lang, subject, message, adapter, reply=''):
        '''push task to queue with null status for execute'''
        stamp = str(datetime.datetime.now())
        cursor = self.dblog.cursor()
        sqlline = ("insert into queue(adapter, reply, transport, recipient, lang, subject, message, stamp, status)"\
                   " values (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\')"
                   % (adapter, reply, transport_name, str(recipient), lang, subject, message, stamp, "NULL"))
        cursor.execute(sqlline)
        self.dblog.commit()
        sqlline = ("select id from queue where adapter=\'%s\' and reply=\'%s\'"\
                   " and  transport=\'%s\' and recipient=\'%s\' and lang=\'%s\' and subject=\'%s\'"\
                   " and message=\'%s\' and stamp=\'%s\';"
                   % (adapter, reply, transport_name, str(recipient), lang, subject, message, stamp))
        id, = cursor.execute(sqlline).fetchone()
        cursor.close()

        self.__try_send(id, adapter, reply, transport_name, recipient, lang, subject, message, stamp)
        return id

    def __restore_queue(self):
        '''fill not done/failed tasks from db'''
        cursor = self.dblog.cursor()
        sqlline = ("select * from queue where status is NULL")
        for t in cursor.execute(sqlline):
            task = (t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7], t[8], 0)
            self.tasksQueue.put(task)

    def __set_status_and_answer(self, id, status=None):
        def _get_status(id):
            '''get task status'''
            cursor = self.dblog.cursor()
            status, reply, = cursor.execute("select status, reply from queue where id=%s" % id).fetchone()
            cursor.close()
            return (status, reply,)
        def _set_status(id, status=None):
            '''set task status'''
            sqlline = "update queue set status=\'%s\' where id=%s;" % (status, id)
            session = self.dblog.cursor()
            session.execute(sqlline)
            self.dblog.commit()
            session.close()
        def _answer(id):
            status, reply, = _get_status(id)
            if reply:
                msg = {"id": id, "status": status}
                self.app._amqp.send_answer(msg, reply)
                log.msg("AMQP. Return result for task id(%s): %s by routing_key %s" % (id, status, reply))
        log.msg("Setting status task_id=%s to %s" % (id, status))
        _set_status(id, status)
        _answer(id)
        return True

    def __failure_send(self, failureMsg, id, adapter, reply,
                       transport_name, recipient, lang,
                       subject, message, stamp, tries):
        '''exception/failure of sendMessage handler'''
        if (str(failureMsg) == "Not supported language to send!") or (str(failureMsg) == "Dispatch failed"):
            self.__set_status_and_answer(id, "Failed")
            self.tasksQueue.task_done()
        else:
            tries += 1
            if tries >= self.tries:
                log.msg("tryes expied. nonawaiting exception: %s." % str(failureMsg))
                self.__set_status_and_answer(id, "Failed")
                self.tasksQueue.task_done()
            else:
                log.msg("nonawaiting exception: %s" % str(failureMsg))
                self.tasksQueue.put((id, adapter, reply, transport_name, recipient,
                                     lang, subject, message, stamp, tries,))

    @defer.inlineCallbacks
    def __try_send(self, id, adapter, reply, transport_name, recipient,
                   lang, subject, message, stamp, tries = 0):
        try:
            yield self.__send_message(transport_name, recipient, lang, subject, message)
            self.__set_status_and_answer(id, "Done")
        except Exception, ex:
            log.msg("Getted exception: %s" % str(ex))
            self.__failure_send(ex.message, id, adapter, reply, transport_name, recipient, lang,
                subject, message, stamp, tries)
        except Failure, fa:
            log.msg("Getted failure: %s" % str(fa))
            self.__failure_send(fa.value, id, adapter, reply, transport_name, recipient, lang,
                subject, message, stamp, tries)

    def __queue_handler(self):
        '''pull from queue, send, check status and send if need result to answerer'''
        while not(self.tasksQueue.empty()):
            q = self.tasksQueue.get()
            (id, adapter, reply, transport_name, recipient, lang, subject, message, stamp, tries) = q
            log.msg("get task id=%s" % id)
            self.__try_send(id, adapter, reply, transport_name, recipient, lang, subject, message, stamp, tries)
        reactor.callLater(_Config_.get("queue_handler_timeout", 10), self.__queue_handler)

    def __send_message(self, transport_name, recipient, lang, subject, message):
        if not lang in ["RUS", "ENG"]:
            raise "Not supported language to send!"
        self._lang = lang
        stamp = str(datetime.datetime.now())
        try:
            type = self.transports[transport_name]["type"]
        except:
            raise Exception("Unknown transport %s. Maybe switched off" % transport_name)
        if type=="plain":
            return self.__send_email(transport_name, recipient, lang, subject, message, stamp)
        if type=="mime":
            return self.__send_email_mime(transport_name, recipient, lang, subject, message, stamp)
        elif type=="jabber":
            return self.__send_jabber(transport_name, recipient, lang, subject, message, stamp)
        elif type=="sms":
            return self.__send_sms(transport_name, recipient, lang, subject, message, stamp)
        elif type=="icq":
            return self.__send_icq(transport_name, recipient, lang, subject, message, stamp)
        else:
            raise Exception("Unknown delivery dispatch %s" % type)

    def __send_success(self, r, stamp, transport_name, recipient, subject):
        transport = self.transports[transport_name]
        if transport["type"] == "jabber":
            if r:
                err = r.getErrorCode()
                if err == "404":
                    raise Exception("XMPP. Error %s. Remote server not found or not valid Jid" % err)
                elif err:
                    raise Exception("XMPP. Error %s. Error with message send" % err)
            else:
                self.__logData(stamp, transport_name, recipient, "Message sent", subject, "OK")
                return  r
        else:
            self.__logData(stamp, transport_name, recipient, "Message sent", subject, "OK")
            return r

    def __send_error(self, f, stamp, transport_name, recipient, lang, subject, message):
        def transportReconnect(transport_name):
            try:
                print "try reconnecting transport %s" % transport_name
                self.app.transport_connect(transport_name)
            except:
                print "error with trying reconnect transport %s" % transport_name

        try:
            f.trap([Failure, Exception])
        except Exception, ex:
            self.__logData(stamp, transport_name, recipient, "Dispatch failed", subject, ex.message)
            log.msg(str(ex))
            if (str(ex.message) == "Disconnected from server."):
                transportReconnect(transport_name)
            raise ex
        except Failure, fa:
            self.__logData(stamp, transport_name, recipient, "Dispatch failed", subject, fa.value)
            log.msg(str(fa))
            if (str(fa.value) == "Disconnected from server."):
                transportReconnect(transport_name)
            raise fa

    def __fin(self, f):
        sys.setdefaultencoding('utf-8')
        return f

    def __send_email(self, transport_name, recipient, lang, subject, message, stamp):
        transport = self.transports[transport_name]

        msg = MIMEText(message.encode("utf-8"))
        msg["Subject"] = subject
        msg["From"] = transport.get("email")
        msg["To"] = recipient
        d = sendmail(
            transport.get("host"),
            transport.get("email"),
            recipient,
            msg.as_string()
        )

        d.addCallback(self.__send_success, stamp, transport_name, recipient, subject)
        d.addErrback(self.__send_error, stamp, transport_name, recipient, lang, subject, message)
        d.addBoth(self.__fin)
        return d

    def __send_email_mime(self, transport_name, recipient, lang, subject, message, stamp):
        transport = self.transports[transport_name]

        msg = message_from_string(message.encode("utf-8"))
        msg["Subject"] = subject
        msg["From"] = transport.get("email")
        msg["To"] = recipient
        d = sendmail(
            transport.get("host"),
            transport.get("email"),
            recipient,
            msg
        )

        d.addCallback(self.__send_success, stamp, transport_name, recipient, subject)
        d.addErrback(self.__send_error, stamp, transport_name, recipient, lang, subject, message)
        d.addBoth(self.__fin)
        return d

    def __send_jabber(self, transport_name, recipient, lang, subject, message, stamp):
        transport = self.transports[transport_name]

        d = threads.deferToThread(transport["transport"].SendAndWaitForResponse, xmpp.protocol.Message(recipient, message.encode("utf-8")))
        if subject:
            subject = subject.encode("utf-8")
        d.addCallback(self.__send_success, stamp, transport_name, recipient, subject)
        d.addErrback(self.__send_error, stamp, transport_name, recipient, lang, subject, message)
        d.addBoth(self.__fin)
        return d


    def __send_icq(self, transport_name, recipient, lang, subject, message, stamp):
        def sendMessage(p, recipient, subject, text):
            p.sendMessage(recipient, text)
            return p

        transport = self.transports[transport_name]

        if self._lang=="RUS":
            sys.setdefaultencoding("cp1251")
            msg = message.encode("cp1251")
        elif self._lang=="ENG":
            msg = message.encode("utf-8")
        d = transport["transport"].protocol.protocol
        d.addCallback(sendMessage, recipient, subject, msg)
        d.addCallback(self.__send_success, stamp, transport_name, recipient, subject)
        d.addErrback(self.__send_error, stamp, transport_name, recipient, lang, subject, message)
        d.addBoth(self.__fin)
        return d

    def __send_sms(self, transport_name, recipient, lang, subject, message, stamp):
        def createPDU(sequence_number, address, recipient, coding, msg):
            pdu = SubmitSM(sequence_number,   #sequence number, need countered
                service_type="",
                source_addr_ton=AddrTon.UNKNOWN,  #only 0
                source_addr_npi=AddrNpi.UNKNOWN,  #only 0
                source_addr=address,  #concord with smsc provider, ignored by provider
                dest_addr_ton=AddrTon.UNKNOWN,    #only 0
                dest_addr_npi=AddrNpi.UNKNOWN,    #only 0
                destination_addr=recipient,
                esm_class=EsmClass(EsmClassMode.DEFAULT, EsmClassType.DEFAULT), #only 0
                protocol_id=0,
                priority_flag=PriorityFlag.LEVEL_0,    #only 0
#                schedule_delivery_time="",
                registered_delivery=RegisteredDelivery(RegisteredDeliveryReceipt.NO_SMSC_DELIVERY_RECEIPT_REQUESTED),  #report delivery: 0 - not need, 1 - need
                replace_if_present_flag=ReplaceIfPresentFlag.DO_NOT_REPLACE,  #0
                data_coding=coding,  #0 for latin, 8 - for cyrillic
                short_message=msg,
            )
            return pdu
        transport = self.transports[transport_name]

        sequence_number = transport["transport"].factory.protocol.claimSeqNum()
        if self._lang=="ENG":
            coding = DataCoding(scheme=DataCodingScheme.DEFAULT, schemeData=DataCodingDefault.SMSC_DEFAULT_ALPHABET)
            msg = message.encode("utf-8")
        elif self._lang=="RUS":
            coding = DataCoding(scheme=DataCodingScheme.DEFAULT, schemeData=DataCodingDefault.UCS2)
            msg = message.encode("utf-16-be")
        pdu = createPDU(sequence_number, transport["source_address"], recipient, coding, msg)
        log.msg("Sending SMS. Recipient %s, subj %s, pdu: %s" % (recipient, subject, pdu))
        d = threads.deferToThread(transport["transport"].factory.protocol.sendPDU, pdu)
        d.addCallback(self.__send_success, stamp, transport_name, recipient, subject)
        d.addErrback(self.__send_error, stamp, transport_name, recipient, lang, subject, message)
        d.addBoth(self.__fin)
        return d

class mSMPPClientFactory(SMPPClientFactory):
    def buildProtocol(self, addr):
        self.protocol = ClientFactory.buildProtocol(self, addr)
        #This is a sneaky way of passing the protocol instance back to the caller
        reactor.callLater(10, self.buildProtocolDeferred.callback, self.protocol)
        return  self.protocol

class mSMPPClientTransceiver(SMPPClientTransceiver):
    def connect(self):
        from smpp.twisted.client import CtxFactory
        self.factory = mSMPPClientFactory(self.config)
        if self.config.useSSL:
            self.log.warning("Establishing SSL connection to %s:%d" % (self.config.host, self.config.port))
            reactor.connectSSL(self.config.host, self.config.port, self.factory, CtxFactory(self.config))
        else:
            self.log.warning("Establishing TCP connection to %s:%d" % (self.config.host, self.config.port))
            reactor.connectTCP(self.config.host, self.config.port, self.factory)
        return self.factory.buildProtocolDeferred.addCallback(self.onConnect)

class UniMessenger(object):
    def __init__(self):
        """Application init"""
        log.msg("Application init")
        self.app_init()
        log.msg("init done")

    def transport_connect(self, sname):
        sender = self.senders[sname]
        if sender["type"]=="plain" and not(sender.get("disabled", False)):
            self._bl.transports[sname]=sender
            log.msg("Added plain text email transport info: %s" % sname)
        elif sender["type"]=="mime" and not(sender.get("disabled", False)):
            self._bl.transports[sname]=sender
            log.msg("Added mime email transport info: %s" % sname)
        elif sender["type"]=="jabber" and not(sender.get("disabled", False)):
            self._bl.transports[sname]= {
                "type": "jabber",
                "service": sender.get("service"),
                "transport": self.connect_jabber(sname, sender)
            }
            timeout = sender.get("keep_alive_timeout", 120)
            reactor.callLater(timeout, self.keep_alive_jabber, timeout, sname)
            log.msg("Preparing jabber-connection done: %s" % sname)
        elif sender["type"]=="sms" and not(sender.get("disabled", False)):
            self.connect_smpp(sname, sender)
            log.msg("Prepearing SMPP-connection %s done" % sname)
        elif sender["type"]=="icq" and not(sender.get("disabled", False)):
            self._bl.transports[sname]= {
                "type": "icq",
                "transport": self.connect_icq(sender)
            }
            reactor.connectTCP(sender.get("host"),int(sender.get("port")), self._bl.transports[sname]["transport"])
            log.msg("Preparing icq-connection done: %s" % sname)

    def connect_jabber(self, transport_name, sender):
        jId = xmpp.protocol.JID(sender.get("username"))
        jabconn = xmpp.Client(sender.get("host"), debug = [])
        jabconn.connect(server=(jId.getDomain(),int(sender.get("port"))))

        if not jabconn.auth(jId.getNode(), sender.get("password")):
            raise IOError('Can not auth with server. %s' % transport_name)
        jabconn._service = self
        return jabconn

    def keep_alive_jabber(self, timeout, transport_name):
        if not self._bl.transports[transport_name]["transport"].isConnected():
            log.msg( "Trnasport %s connection lost....reconnect" % transport_name)
            self._bl.transports[transport_name]["transport"].reconnectAndReauth()
        else:
            transport = self._bl.transports[transport_name]
            ping_msg = ' ' #with this message (_space_) xmpp-dispatcher will ping server in quiet mode
            transport["transport"].send(xmpp.protocol.Message(ping_msg))
        reactor.callLater(timeout, self.keep_alive_jabber, timeout, transport_name)

    def connect_icq(self, sender):
        icqconn = ICQFactory(sender["username"], sender["password"])
        icqconn._service = self
        return icqconn

    @defer.inlineCallbacks
    def connect_smpp(self, transport_name, sender):
        def handleMsg(self, smpp, pdu):
            """NOTE: you can return a Deferred here"""
            log.msg("Received pdu %s" % pdu)
        smppconfig = SMPPClientConfig(host=sender.get("host"),
                                      port=int(sender.get("port")),
                                      username=sender.get("username"),
                                      password=sender.get("password"))
        try:
            #Bind
            self._bl.transports[transport_name] = {
                "type": "sms",
                "source_address": _Config_.get("senders")[transport_name].get("source_address"),
                "transport": mSMPPClientTransceiver(smppconfig, handleMsg)
            }
            self._bl.transports[transport_name]["transport"]._service = self
            smpp = yield self._bl.transports[transport_name]["transport"].connectAndBind()
            #Wait for disconnect
            yield smpp.getDisconnectedDeferred()
        except Exception, e:
            self.logReminder(5, "FATAL! smpp-connection can't set! %s" % str(e))

    def app_init(self):
        """
        First init application procedure contain with config initializing (with
        ConfigParser) and Site init (with Twisted Engine)
        """
        logdest = _Config_.get("logfile")
        if logdest=="stdout":
            log.startLogging(sys.stdout)
        else:
            log.startLogging(open(logdest, "a"))
        logging.basicConfig(level=logging.DEBUG)  #logging for smpp

        try:
            threads = _Config_.get("threads", 2)
        except:
            threads = None
        if threads:
            log.msg("Configuring application for %s threads" % (str(threads)))
            reactor.suggestThreadPoolSize(threads)

        self._bl = BusinessLogic()
        self._bl.app = self

        #db logging init
        self._bl.dblog = sqlite3.connect(_Config_.get("logdb").get("sqlite"))
        self._bl.dblog.execute("create table if not exists queue(id INTEGER PRIMARY KEY AUTOINCREMENT, adapter VARCHAR(6), "\
                               "reply VARCHAR(30), transport VARCHAR(10), recipient VARCHAR(25), lang VARCHAR(5), "\
                               "subject text, message text, stamp VARCHAR(20), status VARCHAR(10));")
        self._bl.dblog.commit()

        #xmlrpc fasade init
        xmlrpc_config = _Config_.get("listeners").get("xmlrpc", None)
        if xmlrpc_config:
            r1 = resource.Resource()
            r1.putChild(xmlrpc_config.get("service"), XmlRpcFacade(self._bl))
            siteXmlrpc = server.Site(r1)
            sslContext = ssl.DefaultOpenSSLContextFactory(xmlrpc_config.get("ssl").get("private-key"),
                                                          xmlrpc_config.get("ssl").get("ssl-certificate"))
            reactor.listenSSL(int(xmlrpc_config.get("port")), siteXmlrpc, contextFactory = sslContext)

        #amqp-rabbitmq fasade init
        if _Config_.get("listeners").get("amqp", None):
            self._amqp = AMQPFasade(self._bl)
        else:
            self._amqp = None
#       this realisation need to complete in future, cause it conflict with current realisation of amqpfasade
#        r2 = resource.Resource()
#        r2.putChild(_Config.get("xmlrpc", "service"), AMQPFasade(self._Config, self._bl))
#        siteAMQP = server.Site(r2)
#        reactor.listenTCP(int(self._Config.get("amqp", "port")), siteAMQP)

        #preparing transports
        self.senders = _Config_.get("senders", None)
        for sname in self.senders.keys():
            try:
                if sname in self._bl.transports.keys(): del self._bl.transports[sname]
                self.transport_connect(sname)
            except Exception, ex:
                log.msg("Cannot connect with transport %s, reason: %s" % (sname, str(ex)))
            except Failure, fa:
                log.msg("Cannot connect with transport %s, reason: %s" % (sname, str(fa)))

    def logReminder(self, sec, msg):
        log.msg(msg)
        reactor.callLater(sec, self.logReminder(sec, msg))

    def run(self):
        """application run"""
        log.msg("Application runs...")
        reactor.run()


def main():
    um = UniMessenger()
    um.run()

if __name__ == "__main__":
    main()
