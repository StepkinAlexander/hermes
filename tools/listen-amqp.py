#!/usr/bin/env python
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters(
    host = '10.0.14.173',
    virtual_host = "hermes",
    credentials = pika.credentials.PlainCredentials('hermes-client', 'password'))
)
channel = connection.channel()
lqueue = 'amq.gen-Thoh3uEzUVkOtf/DcH8ZBg=='

print ' [%s] Waiting for messages. To exit press CTRL+C' % lqueue

def callback(ch, method, properties, body):
    print " [%s] Received %r" % (lqueue, body,)

channel.basic_consume(callback,
    queue=lqueue,
    no_ack=True)

channel.start_consuming()
