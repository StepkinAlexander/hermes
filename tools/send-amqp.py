#!/usr/bin/env python
import pika
import json

connection = pika.BlockingConnection(pika.ConnectionParameters(
               host = 'localhost',
               virtual_host = "hermes",
               credentials = pika.credentials.PlainCredentials('hermes-client', 'password'))
)
channel = connection.channel()

send_rk = 'messenger'
lq = channel.queue_declare()

#msg = {'request': {"transport": "jabber1", "recipient": "aleksandr.stepkin@jabber.brocompany.net", "subj": "t", "text": "test message" }}
msg = {'request': {"transport": "jabber1", "recipient": "aleksandr.stepkin@jabber.brocompany.net", "subj": "t", "text": "test message"}, 'properties': {'reply to': lq.method.queue}}
#msg = {'request': {"transport": "jabber1", "recipient": "left@jabb.net", "subj": "t", "text": "test message"}, 'properties': {'reply to': lq.method.queue}}


channel.basic_publish(exchange='',
    routing_key=send_rk,
    body=json.dumps(msg))
print " [%s] Sent Messages (reply queue %s)" % (send_rk, lq.method.queue)

connection.close()

