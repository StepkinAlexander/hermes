#!/usr/bin/env python

from distutils.core import setup
dist = setup(
    name='hermes',
    version='2.0',
    description = "universal sender",
    long_description = "This service allows to send messeges with many send protocols and messengers",
    author="sas",
    author_email="sastepkin@yandex.ru",
    url="https://dev.brocompany.com/redmine/projects/hermes/",
    license="Broco",

    package_dir={'':'src'},
    packages=['hermes', 'hermes.test'],

    data_files=[("/etc/init.d", ["install/hermes"]),
        ("/etc/hermes", ["src/hermes/config.json-default"]),
        ("/usr/bin",["install/hermesd"]),
        ("/usr/share/hermes", ["src/hermes/amqp0-8.xml"])]
)
