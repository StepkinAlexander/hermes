#
#
#

all: build-deb

prepare:
	mkdir -p build/image

build-deb: clean prepare
	python setup.py bdist_dumb -d build
	cd build/image; tar xf ../*.tar*
	python gendeb.py
clean: 
	rm -rf build
	rm -f *.deb
