#!/usr/bin/env python
#-*- coding:utf-8 -*-

import os

from glob import glob
from py2deb import Py2deb

from subprocess import Popen,check_output,PIPE
from datetime import datetime

p=Py2deb("hermes")

p.author="Stepkin Alexander"
p.mail="secret@mail"
p.description="hermes service"
p.url = "http://"
p.depends="python (>= 2.7), python-twisted, python-setuptools, " \
          "python-txamqp, python-xmpp, "\
          "python-smpp-twisted "
p.license="gpl"
p.section="utils"
p.arch="all"
p.preinstall="install/preinst"
p.postinstall="install/postinst"
p.postremove="install/postrm"

dir_name = os.path.abspath("build/image")

exit
for root, dirs, files in os.walk(dir_name):
    real_dir = "/" + root[len(dir_name):]
    fake_file = []
    for f in files:
        fake_file.append(root + os.sep + f + "|" + f)
    if len(fake_file) > 0:
        p[real_dir] = fake_file
print p

## hash = git rev-parse HEAD
## date in timestamp git show --format="%ci" HEAD | head -n1 

hash = check_output(['git', 'rev-parse', 'HEAD']).rstrip()

p1 = Popen(['git', 'show', '--format="%ct"', 'HEAD'], stdout=PIPE)
p2 = Popen(['head', "-n1"], stdin=p1.stdout, stdout=PIPE)
date_timestamp = p2.communicate()[0].strip("\"\n")

formated_date =  datetime.fromtimestamp(float(date_timestamp)).strftime("%Y%m%d%H%M")

version = "2.0-git%s.%s-vsi" % (formated_date, hash)

build = 1
changeloginformation = "Fixed (large) icon." 

#r = p.generate(version,build,changelog=changeloginformation,tar=True,dsc=True,changes=True,build=False,src=True)
r = p.generate(version)

